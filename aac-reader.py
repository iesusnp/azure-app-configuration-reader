import argparse
import sys
from pathlib import Path
from argparse import ArgumentParser
from azure.appconfiguration import AzureAppConfigurationClient, ConfigurationSetting


def print_config(url, key_filter='*'):
    app_config_client = AzureAppConfigurationClient.from_connection_string(url)
    settings_list = app_config_client.list_configuration_settings(key_filter)
    print("\n ============== [Records] ============== ")
    for item in settings_list:
        print("[" + item.key + "] => " + item.value)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--connection", type=str, required=True,
                        dest="connection_string", help="Connection string provided by Azure")
    parser.add_argument("-f", "--filter", type=str, required=False,
                        dest="key_filter", help="Filter data by the string provider wildcard '*' is allowed")
    args = parser.parse_args()
    print_config(args.connection_string, args.key_filter)

