# Azure App Configuration Reader



## Getting started
1. Install libraries
````
pip install azure-appconfiguration
````
2. Run Application

``
python aac-reader.py -c <CONECTION_STRING> -f <FILTER>
``
3. Done!

#### Example:
    python aac-reader.py -c "Endpoint=https://azure-app-conf.azconfig.io;Id=mHJt-l0-s1:r9xLlPip0iE1kB+9O+H2;Secret=kLs8Yav/K8sYusQVgf9Y7jHs7Hags70LLbuQ+0K7lKc=" -f "azure*"
<mark>Note:</mark> Filter keyword accepts '*' wildcard

